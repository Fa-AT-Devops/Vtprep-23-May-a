using static System.Text.Json.JsonDocument;
using System.Security.Cryptography;
using VTypeUtilDef.Layer;
using VTypeUtilDef;
using System;


namespace VObjectUtilDef;


public partial class VObject :  object

{

    protected VDeviceType? deviceType;
    protected VBusType? busType;
    protected VChannelRange? channelRange;  // ����

    public override string ToString()
    {
        string returnToString = string.Format("VDeviceType ? <{$1}>,VBusType ? <{$2}>, VChannelRange ? <{$3}>",
            this.deviceType, this.busType, this, channelRange);
        return returnToString;
    }

}

public partial class VCommunicateObject:  VObject
{

    protected VPhyLayerConf? phyLayerConf;
    protected VUdpLayerConf? udpLayerConf;
    protected VTransportLayerConf? transportLayerConf;
    protected VTcpLayerConf? tcpLayerConf;

    public VSessionLayerConf? sessionLayerConf;
    public VPresentationLayerConf? presentationLayerConf;

}


public partial class VDriverObject
{

    protected class PrestartConnectionXmlCfg
    {
        
    }
    
    protected class SecurityAccessXmlCfg
    {
        
    }

    protected class ActivateChannelInteractionXmlCfg
    {

    }

    /*protected class ActivateChannelInteractionXmlCfg
    {

    }*/

    protected class WaitReactionXmlCfg
    {

    }

    protected class PredropChannelXmlCfg
    {

    }

    protected class SafeExitXmlCfg
    {

    }
    ///
    ///


    ///
    ///

    protected struct PrestartConnectionJsonConf
    {

    }
    
    protected struct SecurityAccessJsonConf
    {

    }

    protected struct ActivateChannelInteractionJsonConf
    {

    }

    /*protected struct ActivateChannelInteractionJsonConf
    {

    }*/

    protected struct WaitReactionInteractionJsonConf
    {

    }

    protected struct PredropChannelJsonConf
    {

    }

    protected struct SafeExitJsonConf
    {

    }
}