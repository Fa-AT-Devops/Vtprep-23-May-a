namespace VectorUtilApi
{

    public partial class VDriver: VObjectUtilDef.VDriverObject
    {

        private PrestartConnectionXmlCfg? prestartConnectionXmlCfg;
            private PrestartConnectionJsonConf? prestartConnectionJsonConf;

        private SecurityAccessXmlCfg? securityAccesssXmlCfg;
            private SecurityAccessJsonConf? securityAccesssJsonConf;

        private ActivateChannelInteractionXmlCfg? activateChannelInteractionXmlCfg;
            private ActivateChannelInteractionJsonConf? activateChannelInteractionJsonConf;

        

        private WaitReactionXmlCfg? waitReactionInteractionXmlCfg;
            private WaitReactionInteractionJsonConf? waitReactionInteractionJsonConf;

        private PredropChannelXmlCfg? predropChannelXmlCfg;
            private PredropChannelJsonConf? predropChannelJsonConf;

        private SafeExitXmlCfg? safeExitXmlCfg;
            private SafeExitJsonConf? safeExitJsonConf;
    }


}