﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace VTypeUtilDef
{
    public struct VDeviceType
    {

    }

    public struct VBusType
    {

    }

    public struct VChannelRange
    {

    }
}


namespace VTypeUtilDef
{ 
    namespace Layer 
    {
        public struct VPhyLayerConf { }
        public struct VUdpLayerConf { }
        public struct VTransportLayerConf { }
        public struct VTcpLayerConf { }

        public struct VSessionLayerConf { }
        public struct VPresentationLayerConf { }
    }
}